Stone = function(ctx) {
	var lastOrientation = 0;
	var orientation = 0;
	var type = 0;
	var lastXPos = 4;
	var xPos = 4;
	var lastYPos = 1;
	var yPos = 1;
	var ctx = ctx;
	var board = new Array();
	var flashingLinesCount = 0;

	//init Array with 1s at the edges and 0s everywhere else
	for (var row=0; row<21; row++) {
		board[row] = new Array();
		for (var col=0; col<12; col++) {
			if (col === 0 || col === 11 || row === 20) {
				board[row][col] = 1;
			} else {
				board[row][col] = 0;
			}
		}
	}

	var randomizeShape = function() {
		type = Math.floor(Math.random()*7);
		orientation = lastOrientation = 0;
		xPos = lastXPos = 4;
		if (type === 2 || type === 3 || type === 4) {
			yPos = lastYPos = 0;
		} else if (type === 0 || type === 1 || type === 5) {
			yPos = lastYPos = 1;
		} else {
			yPos = lastYPos = 2;
		}

		if (testColission(xPos, yPos, orientation)) {
			ctx.font="bold 45px Verdana";
			ctx.fillStyle="#FFAA00";
			ctx.fillText("Game Over",55,300);
			flashingLinesCount = 9999;
		}
	}

	var drawBlock = function(x, y, c, isStored, isFlashing) {
    //if isFlashing is on, draw brighter blocks 
    var l = 0;
    if (isFlashing) {
    	l = 30;
    }

    //Convert game coordinaes to pixel coordinates
    pixelX = x*40;
    pixelY = y*40;
    
    //Store in Array
    if (isStored) {
    	switch (c) {
    		case 0:
    			board[y][x+1] = 1;
    			break;

    		case 60:
    			board[y][x+1] = 2;
    			break;

    		case 120:
    			board[y][x+1] = 3;
    			break;

    		case 180:
    			board[y][x+1] = 4;
    			break;

    		case 40:
    			board[y][x+1] = 5;
    			break;

    		case 240:
    			board[y][x+1] = 6;
    			break;

    		case 280:
    			board[y][x+1] = 7;
    			break;
    	}
    }
     
    /**** Draw the center part of the block ****/
     
    //Set the fill color using the supplied color

    ctx.fillStyle = "hsl(" + c + ",100%," + (50 + l) + "%)";
     
    //Create a filled rectangle
    ctx.fillRect(pixelX+2,pixelY+2,36,36);
     
 
    /**** Draw the top part of the block ****/
     
    //Set the fill color slightly lighter
    ctx.fillStyle = "hsl(" + c + ",100%," + (70 + l) + "%)";
     
    //Create the top polygon and fill it
    ctx.beginPath();
    ctx.moveTo(pixelX,pixelY);
    ctx.lineTo(pixelX+40,pixelY);
    ctx.lineTo(pixelX+38,pixelY+2);
    ctx.lineTo(pixelX+2,pixelY+2);
    ctx.fill();
     
     
    /**** Draw the sides of the block ****/
     
    //Set the fill color slightly darker
    ctx.fillStyle = "hsl(" + c + ",100%," + (40 + l) + "%)";
     
    //Create the left polygon and fill it
    ctx.beginPath();
    ctx.moveTo(pixelX,pixelY);
    ctx.lineTo(pixelX,pixelY+40);
    ctx.lineTo(pixelX+2,pixelY+38);
    ctx.lineTo(pixelX+2,pixelY+2);
    ctx.fill();
     
    //Create the right polygon and fill it
    ctx.beginPath();
    ctx.moveTo(pixelX+40,pixelY);
    ctx.lineTo(pixelX+40,pixelY+40);
    ctx.lineTo(pixelX+38,pixelY+38);
    ctx.lineTo(pixelX+38,pixelY+2);
    ctx.fill();
     
     
    /**** Draw the bottom part of the block ****/
     
    //Set the fill color much darker
    ctx.fillStyle = "hsl(" + c + ",100%," + (30 + l) + "%)";
     
    //Create the bottom polygon and fill it
    ctx.beginPath();
    ctx.moveTo(pixelX,pixelY+40);
    ctx.lineTo(pixelX+40,pixelY+40);
    ctx.lineTo(pixelX+38,pixelY+38);
    ctx.lineTo(pixelX+2,pixelY+38);
    ctx.fill();
	}

	var clearBlock = function(x, y) {
		//Convert game coordinaes to pixel coordinates
    	pixelX = x*40;
    	pixelY = y*40;

    	//Clear the block
    	ctx.clearRect(pixelX, pixelY, 40, 40);
	}

	var clearStone = function() {
		switch (type) {
			case 0:
				if (lastOrientation === 0) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos - 1);
				} else if (lastOrientation === 1) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos - 1);
				} else if (lastOrientation === 2) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos + 1);
				} else if (lastOrientation === 3) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos - 1);
				}
				break;

			case 1:
				if (lastOrientation === 0) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos - 1);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos - 1, lastYPos + 1);
				} else if (lastOrientation === 1) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos - 1);
				} else if (lastOrientation === 2) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos - 1);
					clearBlock(lastXPos + 1, lastYPos - 1);
					clearBlock(lastXPos + 0, lastYPos + 1);
				} else if (lastOrientation === 3) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 1);
				}
				break;

			case 2:
				if (lastOrientation === 0) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos + 1, lastYPos + 1);
				} else if (lastOrientation === 1) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos - 1);
					clearBlock(lastXPos + 0, lastYPos + 1);
				}
				break;

			case 3:
				if (lastOrientation === 0) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos - 1, lastYPos + 1);
				} 
				break;

			case 4:
				if (lastOrientation === 0) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 1);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos + 1, lastYPos + 0);
				} else if (lastOrientation === 1) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 1);
					clearBlock(lastXPos + 0, lastYPos - 1);
				}
				break;

			case 5:
				if (lastOrientation === 0) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos - 1);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos + 1, lastYPos + 1);
				} else if (lastOrientation === 1) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 1);
				} else if (lastOrientation === 2) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos - 1);
					clearBlock(lastXPos - 1, lastYPos - 1);
					clearBlock(lastXPos + 0, lastYPos + 1);
				} else if (lastOrientation === 3) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos - 1);
				}
				break;

			case 6:
				if (lastOrientation === 0) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 0, lastYPos + 1);
					clearBlock(lastXPos + 0, lastYPos - 1);
					clearBlock(lastXPos + 0, lastYPos - 2);
				} else if (lastOrientation === 1) {
					clearBlock(lastXPos + 0, lastYPos + 0);
					clearBlock(lastXPos + 1, lastYPos + 0);
					clearBlock(lastXPos - 1, lastYPos + 0);
					clearBlock(lastXPos - 2, lastYPos + 0);
				}
				break;
		} 
	}

	this.draw = function(isStored) {
		clearStone();
		switch (type) {
			case 0:
				c = 0;
				if (orientation === 0) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
				} else if (orientation === 1) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
				} else if (orientation === 2) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
				} else if (orientation === 3) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
				}
				break;

			case 1:
				c = 60;
				if (orientation === 0) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos - 1, yPos + 1, c, isStored);
				} else if (orientation === 1) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos - 1, c, isStored);
				} else if (orientation === 2) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
					drawBlock(xPos + 1, yPos - 1, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
				} else if (orientation === 3) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 1, c, isStored);
				}
				break;

			case 2:
				c = 120;
				if (orientation === 0) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos + 1, yPos + 1, c, isStored);
				} else if (orientation === 1) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos - 1, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
				}
				break;

			case 3:
				c = 180;
				if (orientation === 0) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos - 1, yPos + 1, c, isStored);
				} 
				break;

			case 4:
				c = 40;
				if (orientation === 0) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 1, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
				} else if (orientation === 1) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 1, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
				}
				break;

			case 5:
				c = 240;
				if (orientation === 0) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos + 1, yPos + 1, c, isStored);
				} else if (orientation === 1) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 1, c, isStored);
				} else if (orientation === 2) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
					drawBlock(xPos - 1, yPos - 1, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
				} else if (orientation === 3) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos - 1, c, isStored);
				}
				break;

			case 6:
				c = 280;
				if (orientation === 0) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 0, yPos + 1, c, isStored);
					drawBlock(xPos + 0, yPos - 1, c, isStored);
					drawBlock(xPos + 0, yPos - 2, c, isStored);
				} else if (orientation === 1) {
					drawBlock(xPos + 0, yPos + 0, c, isStored);
					drawBlock(xPos + 1, yPos + 0, c, isStored);
					drawBlock(xPos - 1, yPos + 0, c, isStored);
					drawBlock(xPos - 2, yPos + 0, c, isStored);
				}
				break;
		} 
		lastOrientation = orientation;
		lastXPos = xPos;
		lastYPos = yPos;
	}

	var testColission = function(x, y, o) {
		++x;
		//++y;

		switch (type) {
			case 0:
				if (o === 0) {
					if (board[y][x] != 0 || board[y][x-1] != 0 || board[y][x+1] != 0 || board[y-1][x] != 0) {
						return true;
					}
				} else if (o === 1) {
					if (board[y][x] != 0 || board[y+1][x] != 0 || board[y][x+1] != 0 || board[y-1][x] != 0) {
						return true;
					}
				} else if (o === 2) {
					if (board[y][x] != 0 || board[y][x-1] != 0 || board[y][x+1] != 0 || board[y+1][x] != 0) {
						return true;
					}
				} else if (o === 3) {
					if (board[y][x] != 0 || board[y+1][x] != 0 || board[y][x-1] != 0 || board[y-1][x] != 0) {
						return true;
					}
				}
				break;

			case 1:
				if (o === 0) {
					if (board[y][x] != 0 || board[y-1][x] != 0 || board[y+1][x] != 0 || board[y+1][x-1] != 0) {
						return true;
					}
				} else if (o === 1) {
					if (board[y][x] != 0 || board[y][x+1] != 0 || board[y][x-1] != 0 || board[y-1][x-1] != 0) {
						return true;
					}
				} else if (o === 2) {
					if (board[y][x] != 0 || board[y+1][x] != 0 || board[y-1][x] != 0 || board[y-1][x+1] != 0) {
						return true;
					}
				} else if (o === 3) {
					if (board[y][x] != 0 || board[y][x-1] != 0 || board[y][x+1] != 0 || board[y+1][x+1] != 0) {
						return true;
					}
				}
				break;

			case 2:
				if (o === 0) {
					if (board[y][x] != 0 || board[y][x-1] != 0 || board[y+1][x] != 0 || board[y+1][x+1] != 0) {
						return true;
					}
				} else if (o === 1) {
					if (board[y][x] != 0 || board[y+1][x] != 0 || board[y][x+1] != 0 || board[y-1][x+1] != 0) {
						return true;
					}
				}
				break;

			case 3:
				if (o === 0) {
					if (board[y][x] != 0 || board[y][x-1] != 0 || board[y+1][x] != 0 || board[y+1][x-1] != 0) {
						return true;
					}
				}
				break;

			case 4:
				if (o === 0) {
					if (board[y][x] != 0 || board[y][x+1] != 0 || board[y+1][x] != 0 || board[y+1][x-1] != 0) {
						return true;
					}
				} else if (o === 1) {
					if (board[y][x] != 0 || board[y][x+1] != 0 || board[y+1][x+1] != 0 || board[y-1][x] != 0) {
						return true;
					}
				}
				break;

			case 5:
				if (o === 0) {
					if (board[y][x] != 0 || board[y-1][x] != 0 || board[y+1][x] != 0 || board[y+1][x+1] != 0) {
						return true;
					}
				} else if (o === 1) {
					if (board[y][x] != 0 || board[y][x+1] != 0 || board[y][x-1] != 0 || board[y+1][x-1] != 0) {
						return true;
					}
				} else if (o === 2) {
					if (board[y][x] != 0 || board[y+1][x] != 0 || board[y-1][x] != 0 || board[y-1][x-1] != 0) {
						return true;
					}
				} else if (o === 3) {
					if (board[y][x] != 0 || board[y][x-1] != 0 || board[y][x+1] != 0 || board[y-1][x+1] != 0) {
						return true;
					}
				}
				break;

			case 6:
				if (o === 0) {
					if (board[y][x] != 0 || board[y+1][x] != 0 || board[y-1][x] != 0 || board[y-2][x] != 0) {
						return true;
					}
				} else if (o === 1) {
					if (board[y][x] != 0 || board[y][x+1] != 0 || board[y][x-1] != 0 || board[y][x-2] != 0) {
						return true;
					}
				}
				break;

		}

		return false;
	}

	var drawBright = function(row, isFlashing) {
		for (var column = 0; column < 10; column++) {
			clearBlock(column, row);
			if (board[row][column+1] === 1) {
				drawBlock(column, row, 0, false, isFlashing);
			} else if (board[row][column+1] === 2) {
				drawBlock(column, row, 60, false, isFlashing);
			} else if (board[row][column+1] === 3) {
				drawBlock(column, row, 120, false, isFlashing);
			} else if (board[row][column+1] === 4) {
				drawBlock(column, row, 180, false, isFlashing);
			} else if (board[row][column+1] === 5) {
				drawBlock(column, row, 40, false, isFlashing);
			} else if (board[row][column+1] === 6) {
				drawBlock(column, row, 240, false, isFlashing);
			} else if (board[row][column+1] === 7) {
				drawBlock(column, row, 280, false, isFlashing);
			}
		}
	}

	var animateLine = function(row) {
		setTimeout(function() { drawBright(row, true);},1);		
		setTimeout(function() {	drawBright(row, false);},200);
		setTimeout(function() { drawBright(row, true);},400);
		setTimeout(function() {	drawBright(row, false);},600);
		setTimeout(function() { drawBright(row, true);},800);		
		setTimeout(function() {	
			for (; row>=0; row--) {
				if (row-1 >= 0) {
					for (var i=0; i<12; i++) {
						board[row][i] = board[row-1][i];
					}
				} else {
					for (var col=0; col<12; col++) {
						if (col === 0 || col === 11 || row === 20) {
							board[row][col] = 1;
						} else {
							board[row][col] = 0;
						}
					}
				}
				drawBright(row, false);
			}
			--flashingLinesCount;
		},1000);
	}

	var testLines = function() {
		var start;
		var end;

		if (yPos-2 < 0) {
			start = 0;
		} else {
			start = yPos-2;
		}

		if (yPos+1 > 19) {
			end = 19;
		} else {
			end = yPos+1;
		}

		for (; start <= end; start++) {
			for (var column = 1; column < 11; column++) {
				if (board[start][column] === 0) {
					break;
				}
				if (column === 10) {
					flashingLinesCount++;
					animateLine(start);
				}
			}
		}
	}

	this.dropOneRow = function() {
		if (!testColission(xPos, yPos+1, orientation)) {
			++yPos;
		} else {
			this.draw(true);
			testLines();
			randomizeShape();
			this.draw(false);
			return false;
		}
		this.draw(false);
		return true;
	}

	this.drop = function() {
		var droppable = true;
		while (droppable) {
			droppable = this.dropOneRow();
		}
	}

	this.moveLeft = function() {
		if (!testColission(xPos-1, yPos, orientation)) {
			--xPos;
			this.draw(false);
		} 
	}

	this.moveRight = function() {
		if (!testColission(xPos+1, yPos, orientation)) {
			++xPos;
			this.draw(false);
		} 
	}

	this.rotate = function() {
		switch (type) {
			case 0:
			case 1:
			case 5:
				if (orientation < 3) {
					if (!testColission(xPos, yPos, orientation+1)) {
						orientation++;
					}
				} else {
					if (!testColission(xPos, yPos, 0)) {
						orientation = 0;
					}
				}

				this.draw(false);
				break;

			case 2:
			case 4:
			case 6:
				if (orientation < 1) {
					if (!testColission(xPos, yPos, orientation+1)) {
						orientation++;
					}
				} else {
					if (!testColission(xPos, yPos, 0)) {
						orientation = 0;
					}
				}

				this.draw(false);
				break;
		}
	}

	this.isRunning = function() {
		if (flashingLinesCount === 0) {
			return true;
		} else {
			return false;
		}
	}

	randomizeShape();
}
