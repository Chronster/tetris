$().ready(function() {
	canvas = document.getElementById("tetrisCanvas");
    ctx = canvas.getContext("2d");
	var stone = new Stone(ctx);
	stone.draw();

	var date = Date.now();
	var start = date;

	document.addEventListener('keydown', function(event) {
    	if(event.keyCode == 37) {
    		stone.moveLeft();
    	}

    	if(event.keyCode == 39) {
    		stone.moveRight();
    	}

    	if(event.keyCode == 38) {
    		stone.rotate();
    	}

    	if(event.keyCode == 40) {
    		stone.drop();
    	}

	});

	var update = function() {
		
		var now = Date.now();
		var deltaTime = now - date;
		
		//stone.draw();

		date = now;
		if(now - start > 500 && stone.isRunning()) {
			//var pipeUp1 = $('<div class="img-pipe-hole-up"></div>');
			//pipeUp.css('height', '100px');
			//$('.container').append(pipeUp1);
			//score.increase();
			stone.dropOneRow();
			start = now;
		}
		
		requestAnimationFrame(update);
	};
	requestAnimationFrame(update);
});